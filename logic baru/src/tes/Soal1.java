package tes;

public class Soal1 {
	static int getMoneySpent(int[] kacamata, int[] baju, int u) {
         int max=-1;

        for (int i = 0; i < kacamata.length; i++) {
            for (int j = 0; j < baju.length; j++) {
                if (kacamata[i] + baju[j] <= u && kacamata[i] + baju[j] > max)
                    max = kacamata[i] + baju[j];
            }
        }
        return max;

    }

    public static void main(String[] args)  {
    	int u = 50;
    	int[] kacamata = new int[] {26,24,38};
    	int[] baju = new int[] {28,30,27};
    	int max= getMoneySpent(kacamata, baju, u);
    	System.out.println(max);

			
    }
}

