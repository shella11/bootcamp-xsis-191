package hari3;

import java.util.Scanner;

import common.DeretAngka;
import common.PrintArray;

public class Soal1 {
	static Scanner scn;
	
	public static void main(String[] args) {
		scn = new Scanner(System.in);
		System.out.print("Masukkan N : ");
		int n = scn.nextInt();
		System.out.print("Masukkan M : ");
		int m = scn.nextInt();
		System.out.print("Masukkan O : ");
		int o = scn.nextInt();
		
		//1.buat array deret
		int[] deret = DeretAngka.deret01(n*4, m, o);
		
		//2.buat array 2D
		String[][] array = new String[n][n];
		
		//4.membuat index
		int index=0;
		
		//3.isi baris ke 0
		for (int i = 0; i < n; i++) {
			array[0][i]=deret[index]+"";
			index++;
			
		}
		//5.isi kolom ke n-1
		for (int i = 1; i < n; i++) {
			array[i][n-1]=deret[index]+"";
			index++;
		}
		//6. isi baris ke n-1
		for (int i = n-2; i >= 0; i--) {
			array[n-1][i]=deret[index]+"";
			index++;
			
		}
		//7.isi kolom ke 0
		for (int i = n-2; i >0; i--) {
			array[i][0]=deret[index]+"";
			index++;
			
		}
		
		PrintArray.array2D(array);
	}

}
