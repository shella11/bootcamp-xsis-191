package hari3;

import java.util.Scanner;

import common.DeretAngka;
import common.PrintArray;

public class Soal2 {
	static Scanner scn;
	
	public static void main(String[] args) {
		scn= new Scanner(System.in);
		System.out.print("Masukkan nilai N : ");
		int n = scn.nextInt();
		System.out.print("Masukkan nilai M : ");
		int m = scn.nextInt();
		System.out.print("Masukkan nilai O : ");
		int o = scn.nextInt();
		
		int[]vder = DeretAngka.deret01(n*4, m, o);
		
		String[][] array = new String[n][n];
		int index = o;
		//mengisi array
		for (int i = 0; i < n; i++) {
			array[i][n-1-i]="*";
		}
		
		//menampilkan array
		PrintArray.array2D(array);
		
		int[]deret= new int [n*4];
		int angka = m;
		for (int i = 0; i < deret.length; i++) {
			if(i%4==3) {
				deret[i]=angka;
				angka*=3;
			}else {
				deret[i]=angka;
				angka+=m;
				
			}
			System.out.print(deret[i]+" ");
		}
		System.out.println("\n");
	}

}
