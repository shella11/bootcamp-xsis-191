package hari3;

public class Array1 {

	public static void main(String[] args) {
		//deklarasi array 1
		int[] array = new int[10]; //indeks 0-9
		//mengisi elemen value array
		array[0]=1;
		array[1]=2;
		array[2]=3;
		array[3]=4;
		array[4]=5;
		array[5]=6;
		array[6]=7;
		array[7]=8;
		array[8]=9;
		array[9]=1;
		System.out.println(array.length);
		for (int i = 0; i < array.length; i++) {
			System.out.print(array[i]+" ");
		}
		// cara kedua
		int[] aray = new int[] {1,2,3,4,5};
		
		System.out.println(array.length);
		for (int i = 0; i < aray.length; i++) {
			System.out.print(array[i]+" ");
		}
	}

}
