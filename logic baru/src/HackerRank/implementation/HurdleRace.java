package hackerrank.implementation;

public class HurdleRace {
	
	public static int HurdleRace(int k, int[] height) {
		int max = height[0];
		
		for(int i = 1; i<height.length; i++) {
			if(height[i]>max) {
				max=height[i];
			}
		}
		if(k<max) {
			return max-k;
		}else {
			return 0;
		}
	}

	public static void main(String[] args) {
		System.out.println(HurdleRace(4, new int[] {1,6,3,5,2}));

	}

}
