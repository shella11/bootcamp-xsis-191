package hackerrank.implementation;

public class AppleandOranges2 {
	static void countApplesAndOranges(int s, int t, int a, int b, int[] apples, int[] oranges) {
        int merah = 0;
        for(int i = 0; i<apples.length;i++){
            int x=a+apples[i];
            if(x>=s && x<=t){
                merah++;
            }
        }
        System.out.println(merah);

        int jingga = 0;
        for(int i = 0; i<oranges.length;i++){
            int x=b+oranges[i];
            if(x>=s && x<=t){
                jingga++;
            }
        }
       
        System.out.println(jingga);


    }

    public static void main(String[] args) {
    	int s = 5;
    	int t = 25;
    	int a = 8;
    	int b = 87;
    	int []x= {65,-8};
    	int []z= {1,4};
    	countApplesAndOranges(s,t,a,b,x,z);
    }

}
