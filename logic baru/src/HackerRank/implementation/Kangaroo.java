package hackerrank.implementation;

public class Kangaroo {
	static String kangaroo(int x1, int v1, int x2, int v2) {
        String a ="YES";
        if(x1<x2 && v1<v2){
            a="NO";
        }else{
            if(v1!=v2 && (x2-x1)%(v1-v2)==0){
                a="YES";
            }else{
                a="NO";
            }
        }
        return a;
    }

    public static void main(String[] args) {
    	int a1=0;
    	int b1=3;
    	int a2=4;
    	int b2=2;
    	String hasil = kangaroo(a1,b1,a2,b2);
    	System.out.print(hasil+"\n");
    	main1();
    	main2();
    	main3();
    	main4();
    	main5();
    }
    
    //1
    static String kangaroo1(int x1, int v1, int x2, int v2) {
        String b ="YES";
        if(x1<x2 && v1<v2){
            b="NO";
        }else{
            if(v1!=v2 && (x2-x1)%(v1-v2)==0){
                b="YES";
            }else{
                b="NO";
            }
        }
        return b;
    }

    public static void main1() {
    	int c1=14;
    	int d1=10;
    	int c2=9;
    	int d2=7;
    	String hasil = kangaroo1(c1,d1,c2,d2);
    	System.out.print("\nUlang 1 : "+hasil);
    }
    
    //2
    static String kangaroo2(int x1, int v1, int x2, int v2) {
        String c ="YES";
        if(x1<x2 && v1<v2){
            c="NO";
        }else{
            if(v1!=v2 && (x2-x1)%(v1-v2)==0){
                c="YES";
            }else{
                c="NO";
            }
        }
        return c;
    }

    public static void main2() {
    	int e1=11;
    	int f1=45;
    	int e2=4;
    	int f2=3;
    	String hasil = kangaroo2(e1,f1,e2,f2);
    	System.out.print("\nUlang 2 : "+hasil);
    }
    
    //3
    static String kangaroo3(int x1, int v1, int x2, int v2) {
        String d ="YES";
        if(x1<x2 && v1<v2){
            d="NO";
        }else{
            if(v1!=v2 && (x2-x1)%(v1-v2)==0){
                d="YES";
            }else{
                d="NO";
            }
        }
        return d;
    }

    public static void main3() {
    	int g1=14;
    	int h1=4;
    	int g2=98;
    	int h2=2;
    	String hasil = kangaroo3(g1,h1,g2,h2);
    	System.out.print("\nUlang 3 : "+hasil);
    }
    
    //4
    static String kangaroo4(int x1, int v1, int x2, int v2) {
        String e ="YES";
        if(x1<x2 && v1<v2){
            e="NO";
        }else{
            if(v1!=v2 && (x2-x1)%(v1-v2)==0){
                e="YES";
            }else{
                e="NO";
            }
        }
        return e;
    }

    public static void main4() {
    	int i1=52;
    	int j1=77;
    	int i2=6;
    	int j2=12;
    	String hasil = kangaroo4(i1,j1,i2,j2);
    	System.out.print("\nUlang 4 : "+hasil);
    }
    
    //5
    static String kangaroo5(int x1, int v1, int x2, int v2) {
        String f ="YES";
        if(x1<x2 && v1<v2){
            f="NO";
        }else{
            if(v1!=v2 && (x2-x1)%(v1-v2)==0){
                f="YES";
            }else{
                f="NO";
            }
        }
        return f;
    }

    public static void main5() {
    	int k1=95;
    	int l1=5;
    	int k2=7;
    	int l2=52;
    	String hasil = kangaroo5(k1,l1,k2,l2);
    	System.out.print("\nUlang 5 : "+hasil);
    }

}
