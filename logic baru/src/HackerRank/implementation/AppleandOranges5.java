package hackerrank.implementation;

public class AppleandOranges5 {
	static void countApplesAndOranges(int s, int t, int a, int b, int[] apples, int[] oranges) {
        int jadi = 0;
        for(int i = 0; i<apples.length;i++){
            int x=a+apples[i];
            if(x>=s && x<=t){
                jadi++;
            }
        }
        System.out.println(jadi);

        int njeruk = 0;
        for(int i = 0; i<oranges.length;i++){
            int x=b+oranges[i];
            if(x>=s && x<=t){
                njeruk++;
            }
        }
       
        System.out.println(njeruk);


    }

    public static void main(String[] args) {
    	int s = 2;
    	int t = 3;
    	int a = 1;
    	int b = 5;
    	int []m= {2};
    	int []n= {-2};
    	countApplesAndOranges(s,t,a,b,m,n);
    }

}
