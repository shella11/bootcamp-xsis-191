package hackerrank.implementation;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MigratoryBirds {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		migratoryBirds();

	}
	
	 static int migratoryBirds(List<Integer> arr) {
		 Map<Integer, Integer> map = new HashMap();
		 for(int i=0; i<arr.size(); i++) {
			 if(map.containsKey(arr.get(i))) {
				 int n = map.get(arr.get(i));
				 n++;
				 map.put(arr.get(i), n);
			 }else {
				 map.put(arr.get(i), 1);
			 }
		 }
		 
		 int max = 0;
		 int Key = 0;
		 for(Map.Entry<Integer, Integer> item : map.entrySet()) {
			 if(item.getValue()>max) {
				 max = item.getValue();
				 Key = item.getKey();
			 }
			 if(item.getValue()==max && item.getKey()<Key) {
				 Key = item.getKey();
			 }
		 }
		 return Key;
	 }
}
