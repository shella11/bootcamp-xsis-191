package hackerrank.implementation;

public class GradingStudents {
	static int[] gradingStudents(int[] grades) {
         for (int i = 0; i < grades.length; i++){
             if(grades[i]>=38){
                 if(grades[i]%5>=3){
                     grades[i]=grades[i]+(5-grades[i]%5);
                 }
             }
         }
         return grades;
    }

    public static void main(String[] args)  {
    	int[]a= {46,73,67,38,33};
    	int[]result = gradingStudents(a);
    	for (int i=0; i<a.length; i++) {
    		System.out.print(result[i]+" ");
    	}
    	System.out.println("\n\nUlang1");
    	main1();
    	System.out.println("\n\nUlang2");
    	main2();
    	System.out.println("\n\nUlang3");
    	main3();
    	System.out.println("\n\nUlang4");
    	main4();
    	System.out.println("\n\nUlang5");
    	main5();
    }
    //1
    static int[] gradingStudents1(int[] grades) {
         for (int k = 0; k < grades.length; k++){
             if(grades[k]>=38){
                 if(grades[k]%5>=3){
                     grades[k]=grades[k]+(5-grades[k]%5);
                 }
             }
         }
         return grades;
    }

    public static void main1()  {
    	int[]b= {57,68,11,25,64};
    	int[]result = gradingStudents1(b);
    	for (int k=0; k<b.length; k++) {
    		System.out.print(result[k]+" ");
    	}
    }
    //2
    static int[] gradingStudents2(int[]nilai) {
    	for(int q=0; q<nilai.length;q++) {
    		if(nilai[q]>=38) {
    			if(nilai[q]%5>=3) {
    				nilai[q]=nilai[q]+(5-nilai[q]%5);
    			}
    		}
    	}
    	return nilai;
    }
    
    public static void main2() {
    	int[]c= {25,66,74,52,98};
    	int[]hasil=gradingStudents2(c);
    	for(int w=0; w<c.length;w++) {
    		System.out.print(hasil[w]+" ");
    	}
    }
  //3
    static int[] gradingStudents3(int[]jadi) {
    	for(int e=0; e<jadi.length;e++) {
    		if(jadi[e]>=38) {
    			if(jadi[e]%5>=3) {
    				jadi[e]=jadi[e]+(5-jadi[e]%5);
    			}
    		}
    	}
    	return jadi;
    }
    
    public static void main3() {
    	int[]d= {11,54,26,78,3};
    	int[]nilai=gradingStudents3(d);
    	for(int t=0; t<d.length;t++) {
    		System.out.print(nilai[t]+" ");
    	}
    }
  //4
    static int[] gradingStudents4(int[]grade) {
    	for(int r=0; r<grade.length;r++) {
    		if(grade[r]>=38) {
    			if(grade[r]%5>=3) {
    				grade[r]=grade[r]+(5-grade[r]%5);
    			}
    		}
    	}
    	return grade;
    }
    
    public static void main4() {
    	int[]y= {25,24,33,95,41};
    	int[]hasil=gradingStudents4(y);
    	for(int u=0; u<y.length;u++) {
    		System.out.print(hasil[u]+" ");
    	}
    }
  //5
    static int[] gradingStudents5(int[]grad) {
    	for(int o=0; o<grad.length;o++) {
    		if(grad[o]>=38) {
    			if(grad[o]%5>=3) {
    				grad[o]=grad[o]+(5-grad[o]%5);
    			}
    		}
    	}
    	return grad;
    }
    
    public static void main5() {
    	int[]p= {45,85,32,15,94};
    	int[]hasil=gradingStudents5(p);
    	for(int m=0; m<p.length;m++) {
    		System.out.print(hasil[m]+" ");
    	}
    }
}
