package hackerrank.implementation;

public class AppleandOranges3 {
	static void countApplesAndOranges(int s, int t, int a, int b, int[] apples, int[] oranges) {
        int buah1 = 0;
        for(int i = 0; i<apples.length;i++){
            int x=a+apples[i];
            if(x>=s && x<=t){
                buah1++;
            }
        }
        System.out.println(buah1);

        int buah2 = 0;
        for(int i = 0; i<oranges.length;i++){
            int x=b+oranges[i];
            if(x>=s && x<=t){
                buah2++;
            }
        }
       
        System.out.println(buah2);


    }

    public static void main(String[] args) {
    	int s = 7526;
    	int t = 11102;
    	int a = 325;
    	int b = 15;
    	int []apel= {-9,6,1};
    	int []jeruk= {2,6};
    	countApplesAndOranges(s,t,a,b,apel,jeruk);
    }

}
