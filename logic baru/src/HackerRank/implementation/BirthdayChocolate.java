package hackerrank.implementation;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class BirthdayChocolate {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}
	
	static int birthday(List<Integer> s, int d, int m) {
		int count = 0;
		for (int i=0; i<=s.size()-m;i++) {
			int total =0;
			for (int j = 0; j<m; j++) {
				total+=s.get(i+j);
			}
			if(total==d) {
				count++;
			}
		}
		return count;
	}

}
