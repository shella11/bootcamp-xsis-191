package hackerrank.implementation;

public class AppleandOrange {
	static void countApplesAndOranges(int s, int t, int a, int b, int[] apples, int[] oranges) {
        int apel = 0;
        for(int i = 0; i<apples.length;i++){
            int x=a+apples[i];
            if(x>=s && x<=t){
                apel++;
            }
        }
        System.out.println(apel);

        int jeruk = 0;
        for(int i = 0; i<oranges.length;i++){
            int x=b+oranges[i];
            if(x>=s && x<=t){
                jeruk++;
            }
        }
       
        System.out.println(jeruk);


    }

    public static void main(String[] args) {
    	int s = 7;
    	int t = 11;
    	int a = 5;
    	int b = 15;
    	int []apple= {-2,2,1};
    	int []orange= {5,-6};
    	countApplesAndOranges(s,t,a,b,apple,orange);
    }

}
