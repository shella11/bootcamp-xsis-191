package hackerrank.java;

import java.util.ArrayList;
import java.util.List;

public class ComparetheTriplets2 {
	static List<Integer> compareTriplets(List<Integer> a, List<Integer> b) {
		 List<Integer> coba = new ArrayList<Integer>();
	        coba.add(0);
	        coba.add(0);
	        int nD = 0;
	        int nE = 0;
	        for (int i = 0; i<a.size(); i++){
	            if(a.get(i) > b.get(i)){
	                nD++;
	                coba.set(0, nD);
	            }
	            if(a.get(i)<b.get(i)){
	                nE++;
	                coba.set(1, nE);
	            }
	        }
	        return coba;
	    }

	    public static void main(String[] args) {
	    	List<Integer> g = new ArrayList<Integer>();
	    	g.add(45);
	    	g.add(11);
	    	g.add(7);
	    	
	    	List<Integer> h = new ArrayList<Integer>();
	    	h.add(87);
	    	h.add(32);
	    	h.add(54);
	    	
	    	for (Integer hasil : compareTriplets(g,h)) {
	    		System.out.print(hasil + "\t");
	    	}
	    }

}
