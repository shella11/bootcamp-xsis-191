package hackerrank.java;

import java.util.ArrayList;
import java.util.List;

public class ComparetheTriplets4 {
	static List<Integer> compareTriplets(List<Integer> a, List<Integer> b) {
		 List<Integer> ada = new ArrayList<Integer>();
	        ada.add(0);
	        ada.add(0);
	        int nF = 0;
	        int nG = 0;
	        for (int i = 0; i<a.size(); i++){
	            if(a.get(i) > b.get(i)){
	                nF++;
	                ada.set(0, nF);
	            }
	            if(a.get(i)<b.get(i)){
	                nG++;
	                ada.set(1, nG);
	            }
	        }
	        return ada;
	    }

	    public static void main(String[] args) {
	    	List<Integer> l = new ArrayList<Integer>();
	    	l.add(10);
	    	l.add(19);
	    	l.add(15);
	    	
	    	List<Integer> m = new ArrayList<Integer>();
	    	m.add(98);
	    	m.add(74);
	    	m.add(25);
	    	
	    	for (Integer lalu : compareTriplets(l,m)) {
	    		System.out.print(lalu + "\t");
	    	}
	    }

}
