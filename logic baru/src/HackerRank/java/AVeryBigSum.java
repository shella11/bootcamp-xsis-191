package hackerrank.java;

public class AVeryBigSum {

		static long aVeryBigSum (long[] ar) {
			long a = 0;
			for (int i = 0; i < ar.length; i++) {
				a+=ar[i];
			}
			return a;
		}
		public static void main(String[] args) {
			long[]ar=new long[]{1,2,3,4,5};
			long a = aVeryBigSum(ar);
			System.out.println(a);
			main1();
			main2();
			main3();
			main4();
			main5();
		}
		
		//1
		static long aVeryBigSum1 (long[] ya) {
			long b = 0;
			for (int i = 0; i < ya.length; i++) {
				b+=ya[i];
			}
			return b;
		}
		public static void main1() {
			long[]ya=new long[]{5,6,7,4,5};
			long b = aVeryBigSum1(ya);
			System.out.println("\nUlang 1 : "+b);
		}
		
		//2
		static long aVeryBigSum2 (long[] ray) {
			long c = 0;
			for (int i = 0; i < ray.length; i++) {
				c+=ray[i];
			}
			return c;
		}
		public static void main2() {
			long[]ray=new long[]{5,7,4,14,56};
			long c = aVeryBigSum2(ray);
			System.out.println("\nUlang 2 : "+c);
		}
		
		//3
		static long aVeryBigSum3 (long[] ray1) {
			long d = 0;
			for (int i = 0; i < ray1.length; i++) {
				d+=ray1[i];
			}
			return d;
		}
		public static void main3() {
			long[]ray1=new long[]{11,5,6,78,5};
			long d = aVeryBigSum3(ray1);
			System.out.println("\nUlang 3 : "+d);
		}
		
		//4
		static long aVeryBigSum4 (long[] ray2) {
			long e = 0;
			for (int i = 0; i < ray2.length; i++) {
				e+=ray2[i];
			}
			return e;
		}
		public static void main4() {
			long[]ray2=new long[]{32,54,6,25,9};
			long e = aVeryBigSum4(ray2);
			System.out.println("\nUlang 4 : "+e);
		}
		
		//5
		static long aVeryBigSum5 (long[] ray3) {
			long f = 0;
			for (int i = 0; i < ray3.length; i++) {
				f+=ray3[i];
			}
			return f;
		}
		public static void main5() {
			long[]ray3=new long[]{7,65,23,5,45};
			long f = aVeryBigSum5(ray3);
			System.out.println("\nUlang 5 : "+f);
		}
}
