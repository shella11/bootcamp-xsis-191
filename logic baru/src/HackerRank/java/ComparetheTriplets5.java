package hackerrank.java;

import java.util.ArrayList;
import java.util.List;

public class ComparetheTriplets5 {
	static List<Integer> compareTriplets(List<Integer> a, List<Integer> b) {
		 List<Integer> baru = new ArrayList<Integer>();
	        baru.add(0);
	        baru.add(0);
	        int nX = 0;
	        int nZ = 0;
	        for (int i = 0; i<a.size(); i++){
	            if(a.get(i) > b.get(i)){
	                nX++;
	                baru.set(0, nX);
	            }
	            if(a.get(i)<b.get(i)){
	                nZ++;
	                baru.set(1, nZ);
	            }
	        }
	        return baru;
	    }

	    public static void main(String[] args) {
	    	List<Integer> w = new ArrayList<Integer>();
	    	w.add(45);
	    	w.add(11);
	    	w.add(7);
	    	
	    	List<Integer> u = new ArrayList<Integer>();
	    	u.add(87);
	    	u.add(32);
	    	u.add(54);
	    	
	    	for (Integer yey : compareTriplets(w,u)) {
	    		System.out.print(yey + "\t");
	    	}
	    }

}
