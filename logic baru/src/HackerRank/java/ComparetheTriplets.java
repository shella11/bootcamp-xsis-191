package hackerrank.java;

import java.util.List;
import java.util.*;

public class ComparetheTriplets {
	 static List<Integer> compareTriplets(List<Integer> a, List<Integer> b) {
		 List<Integer> result = new ArrayList<Integer>();
	        result.add(0);
	        result.add(0);
	        int nA = 0;
	        int nB = 0;
	        for (int i = 0; i<a.size(); i++){
	            if(a.get(i) > b.get(i)){
	                nA++;
	                result.set(0, nA);
	            }
	            if(a.get(i)<b.get(i)){
	                nB++;
	                result.set(1, nB);
	            }
	        }
	        return result;
	    }

	    public static void main(String[] args) {
	    	List<Integer> c = new ArrayList<Integer>();
	    	c.add(17);
	    	c.add(28);
	    	c.add(30);
	    	
	    	List<Integer> d = new ArrayList<Integer>();
	    	d.add(99);
	    	d.add(16);
	    	d.add(8);
	    	
	    	for (Integer compare : compareTriplets(c,d)) {
	    		System.out.print(compare + "\t");
	    	}
	    }

}
