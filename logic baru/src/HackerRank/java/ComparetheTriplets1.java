package hackerrank.java;

import java.util.List;
import java.util.*;

public class ComparetheTriplets1 {
	static List<Integer> compareTriplets(List<Integer> a, List<Integer> b) {
		 List<Integer> result = new ArrayList<Integer>();
	        result.add(0);
	        result.add(0);
	        int nAl = 0;
	        int nBo = 0;
	        for (int i = 0; i<a.size(); i++){
	            if(a.get(i) > b.get(i)){
	                nAl++;
	                result.set(0, nAl);
	            }
	            if(a.get(i)<b.get(i)){
	                nBo++;
	                result.set(1, nBo);
	            }
	        }
	        return result;
	    }

	    public static void main(String[] args) {
	    	List<Integer> e = new ArrayList<Integer>();
	    	e.add(85);
	    	e.add(51);
	    	e.add(4);
	    	
	    	List<Integer> f = new ArrayList<Integer>();
	    	f.add(66);
	    	f.add(14);
	    	f.add(4);
	    	
	    	for (Integer compare : compareTriplets(e,f)) {
	    		System.out.print(compare + "\t");
	    	}
	    }

}
