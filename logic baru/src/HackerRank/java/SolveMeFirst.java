package hackerrank.java;

public class SolveMeFirst {
	
	static int solveMeFirst(int a, int b) {
    	return a+b;
   }
	    
	 public static void main(String[] args) {
	        int a=3;
	        int b=2;
	        int sum = solveMeFirst(a,b);
	        System.out.println(sum+"\n");
	        main1();
	        main2();
	        main3();
	        main4();
	        main5();
	   }
	 
	 //1
	 static int ulang1(int c, int d) {
		 return c+d;
	 }
	 public static void main1() {
		 int c = 10;
		 int d =6;
		 int hasil = ulang1(c,d);
		 System.out.println("Ulang 1 : "+hasil+"\n");
	 }
	 
	 //2
	 static int ulang2(int e,int f) {
		 return e+f;
	 }
	 public static void main2() {
		 int e = 8;
		 int f =8;
		 int tambah = ulang2(e,f);
		 System.out.println("Ulang 2 : "+tambah+"\n");
	 }
	 
	 //3
	 static int ulang3(int g, int h) {
		 return g+h;
	 }
	 public static void main3() {
		 int g = 45;
		 int h =75;
		 int coba = ulang3(g,h);
		 System.out.println("Ulang 3 : "+coba+"\n");
	 }
	 
	 //4
	 static int ulang4(int i, int j) {
		 return i+j;
	 }
	 public static void main4() {
		 int i = 95;
		 int j = 57;
		 int lagi = ulang4(i,j);
		 System.out.println("Ulang 4 : "+lagi+"\n");
	 }
	 
	 //5
	 static int ulang5(int k, int l) {
		 return k+l;
	 }
	 public static void main5() {
		 int k = 7;
		 int l =23;
		 int z= ulang5(k,l);
		 System.out.println("Ulang 5 : "+z+"\n");
	 }
	}

