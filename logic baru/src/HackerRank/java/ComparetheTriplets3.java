package hackerrank.java;

import java.util.*;

public class ComparetheTriplets3 {
	static List<Integer> compareTriplets(List<Integer> a, List<Integer> b) {
		 List<Integer> hasil = new ArrayList<Integer>();
	        hasil.add(0);
	        hasil.add(0);
	        int nJ = 0;
	        int nL = 0;
	        for (int i = 0; i<a.size(); i++){
	            if(a.get(i) > b.get(i)){
	                nJ++;
	                hasil.set(0, nJ);
	            }
	            if(a.get(i)<b.get(i)){
	                nL++;
	                hasil.set(1, nL);
	            }
	        }
	        return hasil;
	    }

	    public static void main(String[] args) {
	    	List<Integer> j = new ArrayList<Integer>();
	    	j.add(17);
	    	j.add(14);
	    	j.add(32);
	    	
	    	List<Integer> k = new ArrayList<Integer>();
	    	k.add(74);
	    	k.add(19);
	    	k.add(10);
	    	
	    	for (Integer jadi : compareTriplets(j,k)) {
	    		System.out.print(jadi + "\t");
	    	}
	    }

}
