package hari6;

public class Student extends Person {
	//property
	public String major;
	public Double grade;
	
	//constructor
	public Student() {
		super();
		System.out.println("Call Constructor Student");
	}
	//constructor
	public Student(int id, String name, String address, String gender, String major, Double grade) {
		super(id, name, address, gender);
		this.major=major;
		this.grade=grade;
	}
	//method
	public void Info() {
		System.out.println("Id : "+super.id);
		System.out.println("Name : "+super.name);
		System.out.println("Major : "+this.major);
		System.out.println("Grade : "+this.grade);
	}
	public void display() {
		//manggil method person
		super.display();
		System.out.println("Major : "+this.major);
		System.out.println("Grade : "+this.grade);
		
	}

}
