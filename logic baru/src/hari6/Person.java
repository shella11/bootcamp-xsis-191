package hari6;

public class Person {
	//property
	public int id;
	public String name;
	public String address;
	public String gender;
	
	//constructor
	public Person() {
		System.out.println("Call Constructor Person");
		
	}
	//constructor dgn parameter
	public Person(int id, String name, String address, String gender) {
		this.id=id;
		this.name=name;
		this.address=address;
		this.gender=gender;
	}
	//method
	public void display() {
		System.out.println("Id \t: "+this.id);
		System.out.println("Name \t: "+this.name);
		System.out.println("Address : "+this.address);
		System.out.println("Gender \t: "+this.gender);
	}
	

}
