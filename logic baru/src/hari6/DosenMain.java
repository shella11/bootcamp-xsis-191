package hari6;

public class DosenMain {
	
	public static void main(String[] args) {
		Dosen dos1 = new Dosen(1, "Bambang", "Depok", "Pria", 30, "Ekonomi") ;
		System.out.println("Data Dosen 1");
		dos1.tampil();
		
		Dosen dos2 = new Dosen(2,"Gusti", "Bali","Pria", 41,"Matematika");
		System.out.println("\nData Dosen 2");
		dos2.info();

	}
	
}
