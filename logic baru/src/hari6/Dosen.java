package hari6;

public class Dosen extends Person {
	//property
	public int umur;
	public String ngajar;
	
	//constructor
	public Dosen() {
		super();
	}
	//constructor
	public Dosen(int id, String name, String address, String gender, int umur, String ngajar) {
		super(id, name, address, gender);
		this.umur=umur;
		this.ngajar=ngajar;
	}
	//method
	public void info() {
		System.out.println("Id \t: "+super.id);
		System.out.println("Nama \t: "+super.name);
		System.out.println("Mengajar: "+this.ngajar);
	}
	public void tampil() {
		super.display();//manggil method person
		System.out.println("Umur \t: "+this.umur+" tahun");
		System.out.println("Mengajar: "+this.ngajar);
		
	}

}
