package hari6;

public class StudentMain {
	
	public static void main(String[] args) {
		Student std1 = new Student();
		std1.id=1;
		std1.name="Jeje";
		std1.address="Bogor";
		std1.gender="Pria";
		std1.major="Teknik Sipil";
		std1.grade=2.76;
		System.out.println("\nData Student 1");
		std1.display();
		
		Student std2 = new Student(2, "Bejo", "Solo", "Pria", "Teknik Mesin", 2.19) ;
		System.out.println("\nData Student 2");
		std2.display();
		
		Student std3 = new Student(3,"Kinan", "Depok","Wanita", "Akuntansi", 3.11);
		System.out.println("\nData Student 3");
		std3.Info();
		
	}

}
