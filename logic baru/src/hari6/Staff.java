package hari6;

public class Staff extends Person {
	//property
		public int umur;
		public String bagian;
		
		//constructor
		public Staff() {
			super();
		}
		//constructor
		public Staff(int id, String name, String address, String gender, int umur, String bagian) {
			super(id, name, address, gender);
			this.umur=umur;
			this.bagian=bagian;
		}
		//method
		public void info() {
			System.out.println("Id \t: "+super.id);
			System.out.println("Nama \t: "+super.name);
			System.out.println("Staff \t: "+this.bagian);
		}
		public void tampil() {
			super.display();//manggil method person
			System.out.println("Umur \t: "+this.umur+" tahun");
			System.out.println("Staff \t: "+this.bagian);
			
		}

}
