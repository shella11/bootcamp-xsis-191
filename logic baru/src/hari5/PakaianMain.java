package hari5;

public class PakaianMain {
	
	public static void main(String[] args) {
		Pakaian pak1=new Pakaian("L","Kemeja","Kotak-kotak","Flanel");
		System.out.println("Data Pakaian 1");
		pak1.tampil();
		
		Pakaian pak2=new Pakaian("XL","Kaos","Hitam");
		System.out.println("Data Pakaian 2");
		pak2.tampil();
		
		Pakaian pak3= new Pakaian("M","Dress","Maroon","Katun");
		System.out.println("Data Pakaian 3");
		pak3.tampil();
		
		Pakaian pak4=new Pakaian("S","Gamis");
		System.out.println("Data Pakaian 4");
		pak4.tampil();
		
		Pakaian pak5=pak1;
		System.out.println("Data Pakaian 5");
		pak5.tampil();
		
		System.out.println("Data Pakaian 1");
		pak1.tampil();
	}

}
