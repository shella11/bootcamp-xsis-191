package hari5;

public class Kucing {
	//property
	String ras;
	int umur;
	String jk;
	String warna;
	
	//constructor
	public Kucing() {
		
	}
	
	//constructor
	public Kucing(String ras, int umur, String jk, String warna) {
		this.ras=ras;
		this.umur=umur;
		this.jk=jk;
		this.warna=warna;
	}
	
	//constructor
	public Kucing(int umur) {
		this.umur=umur;
	}
	
	//constructor
	public Kucing(String ras) {
		this.ras=ras;
	}
	
	//constructor
	public Kucing(String ras, int umur) {
		this.ras=ras;
		this.umur=umur;
	}
	
	//method
	public void show() {
		System.out.println("Ras \t: "+this.ras);
		System.out.println("Umur \t: "+this.umur+" bulan");
		System.out.println("Jenis Kelamin \t: "+this.jk);
		System.out.println("Warna \t: "+this.warna);
	}

}
