package hari5;

public class Orang {
	//property
	int id;
	String nama;
	String alamat;
	String jk;
	int umur;
	
	//constructor
	public Orang(){
		
	}
	
	//constructor
	public Orang(int id,String nama, String alamat, String jk, int umur) {
		this.id=id;
		this.nama=nama;
		this.alamat=alamat;
		this.jk=jk;
		this.umur=umur;
	}
	//constructor
		public Orang(int id,String nama, String alamat) {
			this.id=id;
			this.nama=nama;
			this.alamat=alamat;
		}
		//method
		public void showData() {
			System.out.println("ID \t:"+this.id);
			System.out.println("Nama \t:"+this.nama);
			System.out.println("Alamat \t:"+this.alamat);
			System.out.println("Jenis Kelamin \t:"+this.jk);
			System.out.println("Umur \t:"+this.umur);
		}

}
