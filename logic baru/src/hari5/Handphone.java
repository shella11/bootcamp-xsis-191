package hari5;

public class Handphone {
	//property
	String merk;
	String warna;
	int ram;
	int kamera;
	
	//constructor
	public Handphone() {
		
	}
	
	//constructor
	public Handphone(String merk, String warna, int ram, int kamera) {
		this.merk=merk;
		this.warna=warna;
		this.ram=ram;
		this.kamera=kamera;
	}
	
	//constructor
	public Handphone(int kamera) {
		this.kamera=kamera;
	}
	//constructor
	public Handphone(String merk, String warna, int ram) {
		this.merk=merk;
		this.warna=warna;
		this.ram=ram;
	}
	//constructor
	public Handphone(String merk,String warna) {
		this.merk=merk;
		this.warna=warna;
	}
	//constructor
	public Handphone(String merk) {
		this.merk=merk;
	}
	//method
	public void Tampil() {
		System.out.println("Merk Hp \t: "+this.merk);
		System.out.println("Warna \t : "+ this.warna);
		System.out.println("RAM \t : "+this.ram+" GB");
		System.out.println("Kamera \t : "+this.kamera+" pixel");
	}

}
