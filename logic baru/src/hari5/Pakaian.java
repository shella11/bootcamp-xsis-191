package hari5;

public class Pakaian {
	//property
	String ukuran;
	String jenis;
	String warna;
	String bahan;
	
	//constructor
	public Pakaian() {
		
	}
	//constructor
	public Pakaian(String ukuran, String jenis, String warna,String bahan) {
		this.ukuran=ukuran;
		this.jenis=jenis;
		this.warna=warna;
		this.bahan=bahan;
	}
	//constructor
	public Pakaian(String ukuran, String jenis) {
		this.ukuran=ukuran;
		this.jenis=jenis;
	}
	//constructor
	public Pakaian(String ukuran,String jenis, String warna) {
		this.ukuran=ukuran;
		this.jenis=jenis;
		this.warna=warna;
	}
	//method
	public void tampil() {
		System.out.println("Ukuran \t: "+this.ukuran);
		System.out.println("Jenis \t: "+this.jenis);
		System.out.println("Warna \t: "+this.warna);
		System.out.println("Bahan \t: "+this.bahan);
	}

}
