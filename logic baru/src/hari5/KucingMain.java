package hari5;

public class KucingMain {
	
	public static void main(String[] args) {
		Kucing kc1 = new Kucing("Scottish",4,"Betina","abu-abu");
		System.out.println("Data Kucing 1");
		kc1.show();
		
		Kucing kc2 = new Kucing("Sphynx",5);
		System.out.println("\nData Kucing 2");
		kc2.show();
		
		Kucing kc3 = new Kucing(6);
		System.out.println("\nData Kucing 3");
		kc3.show();
		
		Kucing kc4 = new Kucing("Savannah");
		System.out.println("\nData Kucing 4");
		kc4.show();
		
		Kucing kc5=kc1;
		System.out.println("\nData Kucing 5");
		kc5.ras="Persia";
		kc5.show();
		
		System.out.println("\nData Kucing 1");
		kc1.show();
	}

}
