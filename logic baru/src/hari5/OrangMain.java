package hari5;

public class OrangMain {
	
	public static void main(String[] args) {
		Orang org1 =new Orang(1, "Yaya", "Bandung", "Wanita", 24);
		System.out.println("Data orang 1");
		org1.showData();
		
		Orang org2 =new Orang(2, "Tina","Bogor");
		System.out.println("\nData orang 2");
		org2.showData();
		
		Orang org3 =new Orang(3, "Dodi", "Depok");
		System.out.println("\nData orang 3");
		org3.showData();
		
		Orang org4=org1;
		System.out.println("\nData Orang 4");
		org4.nama="Indah";
		org4.showData();
		
		System.out.println("\nData orang 1");
		org1.showData();
		
		Orang org5 =org4;
		System.out.println("\nData orang 5");
		org5.jk="Wanita Sholehah";
		org5.showData();
		
		System.out.println("Data orang 1");
		org1.showData();
	
	}

}
