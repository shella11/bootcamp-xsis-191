package hari5;

public class SayuranMain {
	
	public static void main(String[] args) {
		Sayuran yur1= new Sayuran("Daun Singkong","Hijau","umbia-umbian");
		System.out.println("Data Sayur 1");
		yur1.tampil();
		
		Sayuran yur2= new Sayuran("Wortel","Jingga");
		System.out.println("Data Sayur 2");
		yur2.tampil();
		
		Sayuran yur3=yur1;
		System.out.println("Data Sayur 3");
		yur3.nama="Daun Ubi";
		yur3.tampil();
		
		System.out.println("Data Sayur 1");
		yur1.tampil();
	}
	
	

}
