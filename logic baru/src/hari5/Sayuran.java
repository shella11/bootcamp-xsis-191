package hari5;

public class Sayuran {
	//property
	String nama;
	String warna;
	String akar;
	
	//constructor
	public Sayuran() {
		
	}
	
	//constructor
	public Sayuran(String nama, String akar, String warna) {
		this.nama=nama;
		this.akar=akar;
		this.warna=warna;
	}
	
	//contructor
	public Sayuran(String nama, String warna) {
		this.nama=nama;
		this.warna=warna;
	}
	
	//method
	public void tampil() {
		System.out.println("Nama \t: "+this.nama);
		System.out.println("Warna \t: "+this.warna);
		System.out.println("Akar \t: "+this.akar);
	}
	
}
